main : tetris5.o genMino.o MinoI.o MinoS.o MinoL.o MinoT.o MinoSQ.o Mino.o
	g++ -o main tetris5.o genMino.o MinoI.o MinoS.o MinoL.o MinoT.o MinoSQ.o Mino.o

tetris5.o : tetris5.cpp genMino.h
	g++ -c tetris5.cpp

genMino.o : genMino.cpp genMino.h MinoI.h MinoS.h MinoL.h MinoT.h MinoSQ.h Mino.h
	g++ -c genMino.cpp

MinoI.o : MinoI.cpp MinoI.h MinoI.h
	g++ -c MinoI.cpp

MinoS.o : MinoS.cpp MinoS.h Mino.h
	g++ -c MinoS.cpp

MinoL.o : MinoL.cpp MinoL.h Mino.h
	g++ -c MinoL.cpp

MinoT.o : MinoT.cpp MinoT.h Mino.h
	g++ -c MinoT.cpp

MinoSQ.o : MinoSQ.cpp MinoSQ.h Mino.h
	g++ -c MinoSQ.cpp

Mino.o : Mino.cpp Mino.h
	g++ -c Mino.cpp

clean :
	rm main *.o
