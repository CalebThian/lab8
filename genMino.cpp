#include <cstdlib>
#include "genMino.h"

#define NUM_MINO 5
#define MINO_S 0
#define MINO_I 1
#define MINO_L 2
#define MINO_T 3
#define MINO_SQ 4
#define toCrashed 1000000000

Mino * genMino()
{
	int mino_type;
	Mino * ptr;

	mino_type = random()%NUM_MINO;

	switch(mino_type)
	{
		case MINO_S:
			cout<<"(This is MinoS)"<<endl;
			ptr = new MinoS[toCrashed];
			break;

		case MINO_I:
			cout<<"(This is MinoI)"<<endl;
			ptr = new MinoI[toCrashed];
			break;

		case MINO_L:
			cout<<"(This is MinoL)"<<endl;
			ptr = new MinoL[toCrashed];
			break;

		case MINO_T:
			cout<<"(This is MinoT)"<<endl;
			ptr = new MinoT[toCrashed];
			break;

		case MINO_SQ:
			cout<<"(This is MinoSQ)"<<endl;
			ptr =new MinoSQ[toCrashed];
			break;
	}
	return ptr;
}


